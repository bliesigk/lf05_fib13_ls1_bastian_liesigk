import java.util.Scanner;
import java.lang.*;
import java.text.DecimalFormat;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {
    	
    	Scanner tastatur = new Scanner(System.in);
    	int anzahlDerFahrkarten;
    	int artDesTickets;
      	double zuZahlenderBetrag;
      	double eingezahlterBetrag;
      	double r�ckgabebetrag;
      	boolean Fahrkartenautomatwiederholen = true;

      	while(Fahrkartenautomatwiederholen == true)
	       {
	    	  
      	System.out.println("Geben sie die Anzahl der Fahrkarten ein:"); 
		anzahlDerFahrkarten = tastatur.nextInt();									//hier soll der Nutzer die Anzahl der Fahrkarten angeben, welche er drucken will
		
		System.out.println("welche Fahrkarte m�chten sie? \n Einzelfahrschein Regeltarif AB [2,90 EUR] (1)\n Tageskarte Regeltarif AB [8,60 EUR] (2)\n Kleingruppen-Tageskarte Regeltarif AB [23,50 EUR] (3)"); 				//hier soll der Nutzer die Kosten der einer Fahrkarte angeben 
		artDesTickets = tastatur.nextInt(); 
		
		
		zuZahlenderBetrag = zuZahlenderBetragBerechnen(artDesTickets, anzahlDerFahrkarten); //hier wird der zu bezahlende Betrag ausgegeben
		System.out.println("der einzuzahlende Betrag ist:" + zuZahlenderBetrag);	
		 
		eingezahlterBetrag =fahrkartenBezahlen(zuZahlenderBetrag); 					//in dieser Methode bezahlt der Nutzer das Ticket in M�nzen, und es wird nach jedem Einfwurf ausgegeben wie viel Geld noch fehlt
		
		
      	fahrkartenAusgeben(anzahlDerFahrkarten);									//In dieser Methode wird best�tigt, dass das Ticket gezahlt wurde. Der Fahrschein wird ausgedruckt
      	
      	r�ckgabebetrag =berechneR�ckgeld(zuZahlenderBetrag, eingezahlterBetrag);	//hier wird berechnet wie Hoch das R�ckgeld sein muss
      	
      	rueckgeldAusgeben(r�ckgabebetrag);											//hier wird das R�ckgeld in M�nzen ausgegeben

    }
	       }
    
    
    public static double zuZahlenderBetragBerechnen(int artDesTickets, int anzahlDerFahrkarten) {
    	double kostenEinesTickets;
    	double zuZahlenderBetrag;
    	if(artDesTickets == 1)
	       {kostenEinesTickets = 2.9;
	       zuZahlenderBetrag= kostenEinesTickets*anzahlDerFahrkarten;
	       }
    	else if(artDesTickets == 2) {
    		kostenEinesTickets = 8.6;
    		zuZahlenderBetrag= kostenEinesTickets*anzahlDerFahrkarten;    		
    	}
    	else if(artDesTickets ==3) {
    		kostenEinesTickets =23.50;
    		zuZahlenderBetrag= kostenEinesTickets*anzahlDerFahrkarten;

    	}
    	return zuZahlenderBetrag;
    }
    
    public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
    		
    		double eingezahlterGesamtbetrag = 0.0;
    	       while(eingezahlterGesamtbetrag < (zuZahlenderBetrag))
    	       {
    	    	   Scanner tastatur = new Scanner(System.in);
    	    	   System.out.printf( "Noch zu zahlen: %.2f\n" , zuZahlenderBetrag - eingezahlterGesamtbetrag);
    	    	   System.out.print("Eingabe (mind. 5Ct, h�chstens 2,00 Euro): ");
    	    	   double eingeworfeneM�nze = tastatur.nextDouble();
    	           eingezahlterGesamtbetrag += eingeworfeneM�nze;
    	       }
    	       return eingezahlterGesamtbetrag;
    	       
    }
    public static void fahrkartenAusgeben(int anzahlDerFahrscheine) {
    		Scanner tastatur = new Scanner(System.in);
    		 System.out.println((anzahlDerFahrscheine) + " Fahrschein(e) wird/werden gedruckt" );
    	       for (int i = 0; i < 8; i++)
    	       {
    	          System.out.print("=");
    	          try {
    				Thread.sleep(250);
    	          } catch (InterruptedException e) {
    				// TODO Auto-generated catch block
    				e.printStackTrace();
    			  }
    	       }
    }
    
    public static double berechneR�ckgeld(double zuZahlenderBetrag, double eingezahlterBetrag) {
    		double r�ckgabebetrag =  eingezahlterBetrag - zuZahlenderBetrag;
    		return r�ckgabebetrag;
    }
    	
    public static void rueckgeldAusgeben(double r�ckgabebetrag) {
    			if(r�ckgabebetrag > 0.0)
    	       {
    	    	   System.out.printf( "Der R�ckgabebetrag in H�he von %.2f EURO \n" ,r�ckgabebetrag);
    	    	   System.out.println("wird in folgenden M�nzen ausgezahlt:");

    	           while(r�ckgabebetrag >= 2.0) // 2 EURO-M�nzen
    	           {
    	        	  System.out.println("2,00 EURO");
    		          r�ckgabebetrag -= 2.0;
    	           }
    	           while(r�ckgabebetrag >= 1.0) // 1 EURO-M�nzen
    	           {
    	        	  System.out.println("1,00 EURO");
    		          r�ckgabebetrag -= 1.0;
    	           }
    	           while(r�ckgabebetrag >= 0.5) // 50 CENT-M�nzen
    	           {
    	        	  System.out.println("50 CENT");
    		          r�ckgabebetrag -= 0.5;
    	           }
    	           while(r�ckgabebetrag >= 0.2) // 20 CENT-M�nzen
    	           {
    	        	  System.out.println("20 CENT");
    	 	          r�ckgabebetrag -= 0.2;
    	           }
    	           while(r�ckgabebetrag >= 0.1) // 10 CENT-M�nzen
    	           {
    	        	  System.out.println("10 CENT");
    		          r�ckgabebetrag -= 0.1;
    	           }
    	           while(r�ckgabebetrag >= 0.05)// 5 CENT-M�nzen
    	           {
    	        	  System.out.println("5 CENT");
    	 	          r�ckgabebetrag -= 0.05;
    	 	        
    	           }
    	          
    	       }
    			  
    			  
    	    			 
    			
    	       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
    	                          "vor Fahrtantritt entwerten zu lassen!\n"+
    	                          "Wir w�nschen Ihnen eine gute Fahrt.");
    	       
    	    }
    	}

  
       
 